import { useApi } from "../composable/useApi";

const api = useApi();

export async function getProduct(payload: any) {
    try {
        const { data } = await api({
            url: `/product`,
            params: payload
        });

        return data;
    } catch (error) {
        throw error;
    }
}
export async function addProduct(payload: any) {
    try {
        const { data } = await api({
            url: `/product`,
            method: "POST",
            data: payload,
        });

        return data;
    } catch (error) {
        throw error;
    }
}
export async function putProduct(payload: any) {
    try {
        const { data } = await api({
            url: `/product`,
            method: "PUT",
            data: payload,
        });

        return data;
    } catch (error) {
        throw error;
    }
}
export async function selectTypeProduct() {
    try {
        const { data } = await api({
            url: `/product/get-product-types`,
        });

        return data;
    } catch (error) {
        throw error;
    }
}
export async function deleteProduct(id: number) {
    try {
        const { data } = await api({
            url: `/product/${id}`,
            method: "DELETE"
        });

        return data;
    } catch (error) {
        throw error;
    }
}