import axios from "axios";

let api: any = null;

const baseOrgn = import.meta.env.VITE_API_BASE_URL;

export function createApi() {
    api = axios.create({
        baseURL: baseOrgn,
    });
    api.interceptors.request.use((config: any) => {
        config.headers = {
            ...config.headers,
            Accept: "application/json",
        };

        return config;
    });
    api.interceptors.response.use(
        function (response: any) {
            return response;
        },
    );
    return api;
}

export function useApi() {
    if (!api) {
        createApi();
    }
    return api;
}
