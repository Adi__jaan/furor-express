import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import { createPinia } from "pinia";
import { Quasar, Notify } from 'quasar'
const pinia = createPinia();
// Import icon libraries
import '@quasar/extras/material-icons/material-icons.css'
// Import scss
import "./assets/scss/_index.scss";
// Import Quasar css
import 'quasar/src/css/index.sass'
createApp(App)
    .use(pinia)
    .use(Quasar, {
        plugins: {
            Notify
        }, // import Quasar plugins and add here
        // config: {
        //     notify:
        // },
    })
    .mount('#app')
